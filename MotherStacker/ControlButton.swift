//
//  ControlButton.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 9/4/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import SpriteKit

class ControlButton: SKSpriteNode {
    var lane:Lane?
    var enabled:Bool = true {
        didSet {
            self.alpha = (enabled) ? 1.0:0.5
        }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        println("Button Touched")
    }
    
}