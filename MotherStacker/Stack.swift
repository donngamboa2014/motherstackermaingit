//
//  Stack.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 8/30/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import SpriteKit


enum StackSizeType:Int {
    case Smallest = 0, Small, Medium, Large, Largest
    
    var height:CGFloat {
        return 25.0
    }
    
    var width:CGFloat {
        let widthArray:Array<CGFloat> = [100, 120, 140, 160, 180]
        return widthArray[toRaw()]
    }
    
    var color:SKColor {
        let colorArray = [SKColor.whiteColor(), SKColor.blueColor(), SKColor.whiteColor(), SKColor.blueColor(), SKColor.whiteColor()]
            return colorArray[toRaw()]
    }
}

class Stack: SKSpriteNode, Comparable, Equatable {

    var lane:Lane {
        assert(self.parent!.isKindOfClass(Lane), "Parent node must be of class 'Lane'.")
        return self.parent as Lane
    }
    
    var sizeType:StackSizeType {
        didSet {
            self.size.width = self.sizeType.width
        }
    }
    
    var dragged:Bool = false {
        didSet {
            var category:ContactCategory
            if dragged {
                category = ContactCategory.DraggedStack
                physicsBody!.contactTestBitMask = 0x0
            } else if lane.isKindOfClass(Deck) {
                category = ContactCategory.DeckStack
                physicsBody!.contactTestBitMask = 0x0
            } else {
                category = ContactCategory.SettledStack
                physicsBody!.contactTestBitMask = category.toRaw()
            }
            physicsBody!.categoryBitMask = category.toRaw()
            physicsBody!.collisionBitMask = category.toRaw() | ContactCategory.Environment.toRaw()
            physicsBody!.affectedByGravity = (!dragged)
            physicsBody!.velocity = CGVectorMake(0, 0)
            position = CGPointMake(lane.frame.size.width/2, lane.frame.size.height - size.height)
            zPosition = (dragged) ? 10:0
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NS CODING NOT SUPPORTED")
    }
    
    init(texture: SKTexture!, color: UIColor!, size: CGSize, sizeType:StackSizeType) {
        self.sizeType = sizeType
        super.init(texture: texture, color: sizeType.color, size: size)
        setup()
    }
    
    convenience init(sizeType: StackSizeType) {
        let size = CGSizeMake(sizeType.width, sizeType.height)
        self.init(texture: nil, color:sizeType.color, size:size, sizeType:sizeType)
    }
    
    func setup() {
        name = "stack"
        alpha = 0.80
        physicsBody = SKPhysicsBody(rectangleOfSize: self.size)
        physicsBody!.allowsRotation = false
        physicsBody!.restitution = 0
        physicsBody!.categoryBitMask = ContactCategory.DeckStack.toRaw()
        physicsBody!.collisionBitMask = ContactCategory.DeckStack.toRaw() | ContactCategory.Environment.toRaw()
    }
    
    override func removeFromParent() {
        if let index = find(self.lane.straight, self) {
            self.lane.straight.removeAtIndex(index)
        }
        super.removeFromParent()
    }
}


func <(stackA:Stack, stackB:Stack) -> Bool {
    if (stackA.size.width < stackB.size.width) {
        return true
    }
    return false
}

func ==(stackA:Stack, stackB:Stack) -> Bool {
    if (stackA.size.width == stackB.size.width) {
        return true
    }
    return false
}