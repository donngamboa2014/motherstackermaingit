//
//  GameViewController.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 8/28/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import SpriteKit

class GameViewController: UIViewController {
    var scene:GameScene!
    override func viewDidLoad() {
        super.viewDidLoad()
        let documentDirectory:[String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory , NSSearchPathDomainMask.UserDomainMask, true) as [String]
        let path = documentDirectory[0]
        println("\(path)")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.restart()
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func restart() {
        scene = GameScene()
        scene.size = self.view.bounds.size
        let skView = self.view as SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .AspectFit
        scene.gameSceneDelegate = self
        skView.presentScene(scene)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "gameOverSegue") {
            let vc = segue.destinationViewController as GameOverViewController
            vc.delegate = self
            vc.score = scene.score
            vc.level = scene.level
        }
    }
}

extension GameViewController:GameSceneDelegate {
    func presentGameOverMenu() {
        scene.paused = true
        self.performSegueWithIdentifier("gameOverSegue", sender: nil)
    }
}

extension GameViewController:GameOverVCDelegate {
    func startOver() {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.restart()
        })
    }
}