//
//  Lane.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 8/30/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import SpriteKit

class Lane: SKSpriteNode {
    var isAnimating = false
    var straight:Array<Stack> = []
    
    var topStack:Stack? {
        return self.children.last as? Stack
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NS CODING NOT SUPPORTED")
    }
    
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        setup()
    }
    
    convenience init(size: CGSize) {
        self.init(texture: nil, color: SKColor.whiteColor(), size: size)
    }
    
    func updateStraight(stack:Stack) {
        var allStacks = self.children as Array<Stack>
        allStacks.sort(){$0 > $1}
        let lastStack:Int = find(allStacks, stack)!
        for index in 0...lastStack {
            let thisStack:Stack? = allStacks[index]
            if index == 0 {
                straight = [thisStack!]
                continue
            }
            let previousStack:Stack? = allStacks[index - 1]
            if thisStack >= previousStack {
                straight = [thisStack!]
            } else {
                straight.append(thisStack!)
            }
        }
    }
    
    func removeStraight(animated:Bool, completion: ()->()) {
        if isAnimating {
            return
        }
        isAnimating = true
        let animation = SKAction.scaleXTo(0, y: 0, duration: 0.20)
        for stack in straight {
            stack.runAction(animation) {
                stack.removeFromParent()
                self.isAnimating = false
            }
        }
        completion()
    }
    
    func setup() {
        name = "lane"
        physicsBody = SKPhysicsBody(edgeFromPoint: CGPointMake(0, 0), toPoint: CGPointMake(size.width, 0))
        physicsBody!.dynamic = false
        physicsBody!.restitution = 0
        physicsBody!.allowsRotation = false
        physicsBody!.categoryBitMask = ContactCategory.Environment.toRaw()
    }
}

