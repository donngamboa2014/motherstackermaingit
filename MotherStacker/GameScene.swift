//
//  GameScene.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 8/28/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import SpriteKit

let kNumberOfLanes = 3
let kMarginWidth:CGFloat = 100
let kMarginHeight = 200
let kControlButtonMargin:CGFloat = 5
let kControlButtonHeight:CGFloat = 100
let kNumberOfStackTypes = 5
let kLaneColors = [SKColor.lightGrayColor(), SKColor.grayColor(), SKColor.darkGrayColor(), SKColor.greenColor()]
let kComboLimit = 5
let kBaseScore:Float = 100
let kSpawnAnimationDuration = 0.20
let kSpawnDelay = 4.0
let kLevelBonusMultiplier:Float = 0.25
let kLevelSpeedMultiplier:Float = 0.30


enum DeckLevel:Int {
    case VeryLight = 0, Light, Medium, Heavy, VeryHeavy
    var speedAdjustment:Float {
        let speedArray:Array<Float> = [1.0, 0.50, 0, -0.25, -0.50]
        return speedArray[toRaw()]
    }
}

enum ContactCategory:UInt32 {
    case DraggedStack = 1, SettledStack, DeckStack, Environment
}

protocol GameSceneDelegate: NSObjectProtocol {
    func presentGameOverMenu() -> ()
}

class GameScene: SKScene {
    var contentCreated = false
    var lanes:Array<Lane> = []
    var controlButtons:Array<ControlButton> = []
    var stacks:Array<Stack> = []
    var lastLane:Lane?
    var gameSceneDelegate:GameSceneDelegate?
    var dragStack:Stack? {
        didSet {
            oldValue?.dragged = false
            dragStack?.dragged = true
        }
    }
    var score:Float = 0 {
        willSet {
            switch (newValue) {
            case 0..<300:
                level = 1
            case 300..<1000:
                level = 2
            case 1000..<2000:
                level = 3
            case 2000..<4000:
                level = 4
            case 4000..<7000:
                level = 5
            case 7000..<10000:
                level = 6
            case 10000..<15000:
                level = 7
            case 15000..<22000:
                level = 8
            case 22000..<30000:
                level = 9
            default:
                level = 10
            }
        }
        didSet {
            if let labelNode = self.childNodeWithName("scoreLabel") as? SKLabelNode {
                labelNode.text = NSString(format: "Score: %.0f", score)
            }
            updateSpeed()
        }
    }
    
    var level:Int = 1{
        didSet {
            if let labelNode = self.childNodeWithName("levelLabel") as? SKLabelNode {
                labelNode.text = "Level: \(level)"
            }
        }
    }
    
    override func didMoveToView(view: SKView) {
        if !contentCreated {
            createContents()
            contentCreated = true
        }
    }
    
    func createContents() {
        self.backgroundColor = SKColor.blackColor()
        self.physicsWorld.gravity = CGVectorMake(0, -50.0)
        self.physicsWorld.contactDelegate = self
        
        self.createScoreLabel()
        self.createLevelLabel()
        self.createLanes()
        self.createDeck()
        self.createControlButtons()
        
        self.spawnStacks()
    }
    
    func createLanes() {
        for x in 0..<kNumberOfLanes {
            
            let kLaneLayerWidth = (self.size.width - CGFloat(kMarginWidth) * CGFloat(2)) / CGFloat(kNumberOfLanes+1)
            let kLaneLayerHeight = (self.size.height - CGFloat(kMarginHeight) * CGFloat(2))
            let lane = Lane(size:CGSizeMake(kLaneLayerWidth, kLaneLayerHeight))
            
            lane.anchorPoint = CGPointZero
            lane.position = CGPointMake(CGFloat(kMarginWidth) + CGFloat(x) * lane.size.width, CGFloat(kMarginHeight))
            lane.color = kLaneColors[x]
            lanes.append(lane)
            addChild(lane)
        }
    }
    
    func createDeck() {
        let kLaneLayerWidth = (self.size.width - CGFloat(kMarginWidth) * CGFloat(2)) / CGFloat(kNumberOfLanes+1)
        let kLaneLayerHeight = (self.size.height - CGFloat(kMarginHeight) * CGFloat(2))
        let deck = Deck(size: CGSizeMake(kLaneLayerWidth, kLaneLayerHeight))
        
        deck.anchorPoint = CGPointZero
        deck.position = CGPointMake(CGFloat(kMarginWidth) + CGFloat(kNumberOfLanes) * deck.size.width, CGFloat(kMarginHeight))
        deck.color = SKColor.greenColor()
        
        addChild(deck)
    }
    
    func createScoreLabel() {
        let label = SKLabelNode(fontNamed: "Futura")
        label.text = "Score: 0"
        label.position = CGPointMake(CGFloat(self.frame.size.width * 1/4), self.size.height - CGFloat(kMarginHeight/2))
        label.name = "scoreLabel"
        self.addChild(label)
    }
    
    func createLevelLabel() {
        let label = SKLabelNode(fontNamed: "Futura")
        label.text = "Level: \(level)"
        label.position = CGPointMake(CGFloat(self.frame.size.width * 3/4), self.size.height - CGFloat(kMarginHeight/2))
        label.name = "levelLabel"
        self.addChild(label)
    }
    
    func createControlButtons() {
        for x in 0..<kNumberOfLanes+1 {
            let kButtonWidth = (self.size.width - kMarginWidth * CGFloat(2)) / CGFloat(kNumberOfLanes+1) - kControlButtonMargin * CGFloat(2)
            let button = ControlButton(color: SKColor.darkGrayColor(), size: CGSizeMake(kButtonWidth, kControlButtonHeight))
            
            button.anchorPoint = CGPointZero
            button.position = CGPointMake(kMarginWidth + CGFloat(x) * button.size.width + kControlButtonMargin * CGFloat(x*2+1) , 80)
            if x == kNumberOfLanes {
                button.lane = self.childNodeWithName("deck") as? Lane
            } else {
                button.lane = lanes[x]
            }
            controlButtons.append(button)
            addChild(button)
        }
    }
    
    func createStack() {
        let deck = self.childNodeWithName("deck")!
        
        let stack = Stack(sizeType: randomStackType())
        stacks.append(stack)
        stack.position = CGPointMake(deck.frame.size.width/2, 0)
        
        let label = SKLabelNode(fontNamed: "Futura")
        label.text = String(stack.sizeType.toRaw()+1)
        label.fontSize = 16
        label.position = CGPoint(x: 0, y: -label.frame.size.height/2)
        label.fontColor = (stack.color == SKColor.blueColor()) ? SKColor.whiteColor(): SKColor.blueColor()
        stack.addChild(label)
        
        deck.addChild(stack)
        let action = SKAction.moveToY(stack.frame.size.height, duration: kSpawnAnimationDuration)
        stack.runAction(action)
    }
    
    func spawnStacks() {
        let deck = self.childNodeWithName("deck")!
        let moveUp:()->() = {
            deck.enumerateChildNodesWithName("stack", usingBlock: { (node, stop) -> Void in
                let stack = node as Stack
                if !stack.dragged {
                    let riseByHeight = SKAction.moveByX(0, y: 100, duration: kSpawnAnimationDuration)
                    node.runAction(riseByHeight)
                }
            })
        }
        
        let spawnNewStacks = SKAction.sequence([SKAction.runBlock(moveUp), SKAction.runBlock(createStack), SKAction.runBlock(checkGameOver), SKAction.runBlock(updateSpeed), SKAction.waitForDuration(kSpawnDelay)])
        self.runAction(SKAction.repeatActionForever(spawnNewStacks))
    }
}


//CONTACT DELEGATE
extension GameScene: SKPhysicsContactDelegate {
    func didBeginContact(contact: SKPhysicsContact!) {
        if lastLane == nil {
            return
        }
        
        if (contact.bodyA.categoryBitMask & ContactCategory.SettledStack.toRaw() > 0) &&
            (contact.bodyB.categoryBitMask & ContactCategory.SettledStack.toRaw() > 0) {
                let stackA = contact.bodyA.node as Stack
                let stackB = contact.bodyB.node as Stack
                let stack = (stackA < stackB) ? stackA:stackB
                let lane = stack.lane
                lane.updateStraight(stack)
                if lane.straight.count == kComboLimit {
                    lane.removeStraight(true) {
                        self.removeStacksInRecord(lane.straight)
                        self.incrementScoreByLevel()
                    }
                }
        }
    }
}

extension GameScene {
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.anyObject() as UITouch
        let point = touch.locationInNode(self)
        let button = self.nodeAtPoint(point) as? ControlButton
        if button == nil {
            return
        }
        
        let lane = button!.lane!
        if dragStack == nil {
            if let topStack = lane.topStack {
                dragStack = topStack
                lastLane = lane
                button!.alpha = 0.5
            }
        } else {
            if lastLane == lane {
            } else if checkForSwapValidity(dragStack!, lane: lane) {
                dragStack!.removeFromParent()
                lane.addChild(dragStack!)
            }
            dragStack = nil
            makeAllButtonsVisible()
        }
    }
}

//HELPER METHODS
extension GameScene {
    func getLeastCommonStackType() -> Array<Int> {
        var leastCountTypes:Array<Int> = []
        var leastCount = 1000000
        for x in 0 ... kNumberOfStackTypes-1 {
            var count = 0
            for stack in stacks {
                if stack.size.width == StackSizeType.fromRaw(x)!.width {
                    count++
                }
            }
            if count < leastCount {
                leastCountTypes = [x]
                leastCount = count
            } else if count == leastCount && !contains(leastCountTypes, x){
                leastCountTypes.append(x)
            }
        }
        return leastCountTypes
    }
    
    func checkGameOver() {
        let deck = self.childNodeWithName("deck")!
        
        if deck.children.count > 14 {
            self.gameSceneDelegate?.presentGameOverMenu()
        }
    }
    
    func checkForSwapValidity(stack:Stack, lane:Lane)->Bool {
        if (lane.topStack == nil || lane.topStack >= stack) && lane.name != "deck" {
            return true
        } else {
            return false
        }
    }
    
    func makeAllButtonsVisible() {
        for button in controlButtons {
            button.alpha = 1.0
        }
    }
    
    func removeStacksInRecord(stackArray:Array<Stack>)->() {
        for stack in stackArray {
            if let index = find(stacks, stack) {
                stacks.removeAtIndex(index)
            }
        }
    }
}

//GAME AND LEVEL LOGIC
extension GameScene {
    func incrementScoreByLevel() {
        let earnedPoints = kBaseScore * (1.0 + Float(level - 1) * kLevelBonusMultiplier)
        score += earnedPoints
    }
    
    func randomStackType() -> StackSizeType {
        var randomType:StackSizeType
        let leastCountTypes:Array = self.getLeastCommonStackType()
        let stragglerChance:Float = 1.0 / (11.0 - Float(level))
        let stragglerBase:UInt32 = UInt32(1.0 / stragglerChance)
        println("Spawning... with straggler chance: \(stragglerChance)")
        
        let stragglerType = (arc4random_uniform(stragglerBase)==stragglerBase-1)
        var checker:Bool
        println("Result... straggler: \(stragglerType)")
        do {
            let randomNumber = Int(arc4random_uniform(5))
            randomType = StackSizeType.fromRaw(randomNumber)!
            if (leastCountTypes.count==5) {
                checker = false
            } else {
                checker = !contains(leastCountTypes, randomType.toRaw())
                checker = (stragglerType) ? !checker:checker
            }
        } while (checker)
        
        return randomType
    }
    func updateSpeed() {
        let deck = self.childNodeWithName("deck")!
        var deckLevel:DeckLevel
        switch (deck.children.count) {
        case 10..<14:
            deckLevel = .VeryHeavy
        case 8..<10:
            deckLevel = .Heavy
        case 6..<8:
            deckLevel = .Medium
        case 4..<6:
            deckLevel = .Light
        default:
            deckLevel = .VeryLight
        }
        
        let speed = CGFloat(1 + Float(level - 1) * kLevelSpeedMultiplier) * CGFloat(Float(1.0) + deckLevel.speedAdjustment)
        self.speed = speed
        
        println("Number of elements in stack array: \(stacks.count)")
    }
}

