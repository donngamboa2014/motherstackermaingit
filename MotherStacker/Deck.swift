//
//  Deck.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 8/30/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import SpriteKit

let kDefaultDeckColor = SKColor.whiteColor()

class Deck: Lane {
    override var topStack:Stack? {
        return self.children.first as? Stack
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NS CODING NOT SUPPORTED")
    }
    
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        setup()
    }
    
    override func updateStraight(stack: Stack) {
        println("Error: straight function should be disable at deck")
    }
    
    override func removeStraight(animated: Bool, completion: () -> ()) {
        println("Error: straight function should be disable at deck")
    }
    
    override func setup() {
        super.setup()
        name = "deck"
    }
}
