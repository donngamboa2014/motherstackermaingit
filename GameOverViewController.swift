//
//  GameOverViewController.swift
//  BooksOfHanoi
//
//  Created by WC-Donn on 8/29/14.
//  Copyright (c) 2014 White Cloak. All rights reserved.
//

import UIKit

protocol GameOverVCDelegate {
    func startOver()
}

class GameOverViewController: UIViewController {
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var delegate:GameOverVCDelegate?
    var score:Float?
    var highScore:Float?
    var level:Int?
    var data:NSMutableDictionary
    var path:String!
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var highScoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let fileManager = NSFileManager.defaultManager()
        let documentDirectory:[String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory , NSSearchPathDomainMask.UserDomainMask, true) as [String]
        path = documentDirectory[0] + "/score.plist"
        if fileManager.fileExistsAtPath(path) {
            data = NSMutableDictionary(contentsOfFile: path)
            highScore = data.valueForKey("highScore")!.floatValue
        } else {
            data = NSMutableDictionary()
            highScore = 0
        }
        checkHighScore()
    }
    
    @IBAction func startOverTapped(sender: AnyObject) {
        delegate?.startOver()
    }
    
    override func viewDidLayoutSubviews() {
        label!.text = NSString(format: "%.0f at level %i", score!, level!)
        highScoreLabel.text = NSString(format: "Best: %.0f", highScore!)
    }
    
    func checkHighScore() {
        if score > highScore {
            data.setValue(NSNumber.numberWithFloat(score!), forKey: "highScore")
            data.writeToFile(path, atomically: true)
        }
    }
    
    deinit {
        delegate = nil
    }
}
