//
//  swipefunction.swift
//  MotherStacker
//
//  Created by Donn Carlo on 9/6/14.
//  Copyright (c) 2014 Donn Carlo. All rights reserved.
//

import Foundation


//RESPONDER METHODS
//extension GameScene {
//    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
//        if touches.count > 1 || self.dragStack != nil {
//            return
//        }
//        let touch: AnyObject! = touches.anyObject()
//        let point = touch.locationInNode(self)
//        let lane = getLaneInPoint(point)
//        let deck = getDeckInPoint(point)
//
//        if lane == nil && deck == nil {
//            return
//        }
//
//        if let topStack = lane?.topStack {
//            dragStack = topStack
//            dragStack.dragged = true
//        } else if deck != nil { //Work around for a bug in Xcode beta 6: Optional binding in overriden methods causes ambigious value errors
//            if let topStack = (deck! as Lane).topStack {
//                dragStack = topStack
//                dragStack.dragged = true
//            }
//        }
//    }
//
//    override func touchesMoved(touches: NSSet!, withEvent event: UIEvent!) {
//        if touches.count > 1 || dragStack == nil {
//            return
//        }
//        let touch: AnyObject! = touches.anyObject()
//        let point = touch.locationInNode(self)
//        let lane = getLaneInPoint(point)
//
//        if lane == nil || dragStack.lane == lane {
//            return
//        }
//
//        if checkForSwapValidity(dragStack, lane: lane!) {
//            dragStack.removeFromParent()
//            lane!.addChild(dragStack)
//        }
//    }
//
//    override func touchesEnded(touches: NSSet!, withEvent event: UIEvent!) {
//        if dragStack == nil {
//            return
//        }
//        let movedStack = dragStack
//        dragStack.dragged = false
//        dragStack = nil
//        lastLane = movedStack.parent? as? Lane
//    }
//
//    override func touchesCancelled(touches: NSSet!, withEvent event: UIEvent!) {
//        touchesEnded(touches, withEvent: event)
//    }
//}